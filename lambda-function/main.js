'use strict'

exports.handler = function (event, context, callback) {
  var response = {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/html; charset=utf-8'
    },
    body: '<h1>This is two hello-terraform-serverless.</h1><p><a href="https://gitlab.com/willhallonline/hello-terraform-serverless">Hello Terraform Serverless on GitLab</a><br />Have a great rest of the day. I\'ve been Will Hall :) </p>'
  }
  callback(null, response)
}
