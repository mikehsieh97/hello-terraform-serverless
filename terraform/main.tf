provider "aws" {
  region  = "us-east-1"
  version = "~> 2.0"
}

module "lambda_serverless_no_tag" {
  source = "./modules/lambda-serverless-node10"

  function_name     = "hello-serverless-no-tag"
  describe_function = "Hello Serverless Terraform no tag"

  subdomain = "na"
  domain    = "go.willhallonline.net"

  s3_key    = "hello.zip"
  s3_bucket = "terraform-hello-12345"
}

/*
module "lambda_serverless_v0_1_6" {
  source = "./modules/lambda-serverless-node10"

  function_name     = "hello-serverless-v0-1-6"
  describe_function = "Hello Serverless Terraform v0.1.6"

  subdomain = "v0-1-6"
  domain    = "go.willhallonline.net"

  s3_key    = "v0.1.6/hello.zip"
  s3_bucket = "terraform-hello-12345"
}

module "lambda_serverless_v0_1_7" {
  source = "./modules/lambda-serverless-node10"

  function_name     = "hello-serverless-v0-1-7"
  describe_function = "Hello Serverless Terraform v0.1.7"

  subdomain = "v0-1-7"
  domain    = "go.willhallonline.net"

  s3_key    = "v0.1.7/hello.zip"
  s3_bucket = "terraform-hello-12345"
}

module "lambda_serverless_v0_1_8" {
  source = "./modules/lambda-serverless-node10"

  function_name     = "hello-serverless-v0-1-8"
  describe_function = "Hello Serverless Terraform v0.1.8"

  subdomain = "v0-1-8"
  domain    = "go.willhallonline.net"

  s3_key    = "v0.1.8/hello.zip"
  s3_bucket = "terraform-hello-12345"
}

module "lambda_serverless_v0_1_9" {
  source = "./modules/lambda-serverless-node10"

  function_name     = "hello-serverless-v0-1-9"
  describe_function = "Hello Serverless Terraform v0.1.9"

  subdomain = "v0-1-9"
  domain    = "go.willhallonline.net"

  s3_key    = "v0.1.9/hello.zip"
  s3_bucket = "terraform-hello-12345"
}
*/