variable "region" {
  type        = string
  description = "AWS region for deployment"
  default     = "us-east-1"
}

variable "function_name" {
  type        = string
  description = "Function name"
  default     = "hello-world"
}

variable "s3_bucket" {
  type        = string
  description = "The S3 bucket that holds the packaged function"
  default     = "terraform-hello-12345"
}

variable "s3_key" {
  type        = string
  description = "The key location for the serverless package"
  default     = "hello.zip"
}

variable "subdomain" {
  type        = string
  description = "Subdomain of the function URL"
  default     = "v0-1-1"
}

variable "handler" {
  type        = string
  description = "The handler for the serverless invocation"
  default     = "main.handler"
}

variable "runtime" {
  type        = string
  description = "The handler runtime for serverless invocation"
  default     = "nodejs10.x"
}

variable "domain" {
  type    = string
  default = "go.willhallonline.net"
}

variable "describe_function" {
  type        = string
  description = "A description of the function. Very inception."
  default     = "Describe me."
}

variable "edge_cert_arn" {
  type    = string
  default = "arn:aws:acm:us-east-1:525339921445:certificate/92becfc8-cf53-413b-af9e-4adb9653f7d1"
}
